import { combineReducers } from "redux";
import homeReducer, { HomeState } from "../../modules/Home/Home.Reducer";

const rootReducer = combineReducers({
  home: homeReducer,
});

export default rootReducer;

export interface RootState {
  home: HomeState;
}
