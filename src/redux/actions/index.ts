export enum HomeActions {
  SAVE_LIST_PRODUCT = "SAVE_LIST_PRODUCT",
}

export enum ShopActions {
  SAVE_SHOP_PROFILE = "SAVE_SHOP_PROFILE",
  SAVE_SHOP_PRODUCT = "SAVE_SHOP_PRODUCT",
}

export enum AuthActions {
  SIGN_IN = "SIGN_IN",
  SIGN_OUT = "SIGN_OUT",
}

export enum AdminActions {
  SAVE_LIST_ACCOUNT = "SAVE_LIST_ACCOUNT",
  SAVE_LIST_SHOP = "SAVE_LIST_SHOP",
  SAVE_FILTER = "SAVE_FILTER",
}
