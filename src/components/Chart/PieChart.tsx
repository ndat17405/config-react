import React from "react";
import { Chart } from "react-google-charts";

function PieChart({ title, Data }: { title: string; Data: any }) {
  const options = {
    title: title,
    is3D: true,
  };
  return (
    <Chart
      chartType="PieChart"
      data={Data}
      options={options}
      width={"100%"}
      height={"400px"}
    />
  );
}

export default PieChart;
