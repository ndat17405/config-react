import React from "react";
import { Chart } from "react-google-charts";

function ColumnChart({
  title,
  data,
  size,
}: {
  title: string;
  data: any;
  size: string;
}) {
  const options = {
    title: title,
  };

  return (
    <Chart
      chartType="ColumnChart"
      width={size}
      height="400px"
      data={data}
      options={options}
    />
  );
}

export default ColumnChart;
