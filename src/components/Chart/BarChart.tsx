import React from "react";
import { Chart } from "react-google-charts";

function BarChart({ title, data }: { title: string; data: any }) {
  const options = {
    title: title,
    width: 600,
    height: 400,
    bar: { groupWidth: "70%" },
    legend: { position: "none" },
  };

  return (
    <Chart
      chartType="BarChart"
      width="100%"
      height="400px"
      data={data}
      options={options}
    />
  );
}

export default BarChart;
