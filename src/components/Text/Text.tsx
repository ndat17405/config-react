import React from "react";

function Text({
  title,
  property,
  onPress,
}: {
  title: any;
  property?: any;
  onPress?: any;
}) {
  return (
    <p className={property} onClick={onPress}>
      {title}
    </p>
  );
}

export default Text;
