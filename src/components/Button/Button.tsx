import React from "react";
import "./Button.scss";

function Button({
  title,
  onPress,
  property,
}: {
  title: any;
  onPress?: any;
  property: any;
  isDisable?: any;
}) {
  return (
    <button className={property} onClick={onPress}>
      {title}
    </button>
  );
}

export default Button;
