import React from "react";

function TextField({
  title,
  type,
  property,
  value,
  onChange,
}: {
  title?: any;
  type?: any;
  property?: any;
  value?: any;
  onChange?: any;
}) {
  return (
    <input
      type={type}
      className={property}
      placeholder={title}
      value={value}
      onChange={onChange}
    />
  );
}

export default TextField;
