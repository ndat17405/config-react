import React from "react";
import "./ImageAsset.scss";

function ImageAsset({ source, property }: { source: any; property: any }) {
  return <img src={source} alt="?" className={property} />;
}

export default ImageAsset;
