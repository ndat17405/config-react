import React from "react";
import "./Box.scss";

function BoxContainer({
  children,
  property,
}: {
  children: any;
  property: any;
}) {
  return (
    <div className={property === "view" ? "box-container" : property}>
      {children}
    </div>
  );
}

export default BoxContainer;
