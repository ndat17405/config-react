export const capitalizeString = (str: string): string => {
    if (!str) return '';
    return str.charAt(0).toUpperCase() + str.slice(1);
};

export const formatNumber = (num: number): string => {
    if (!num) return '0';
    return num.toLocaleString();
};

export const formatDate = (date: Date): string => {
    if (!date) return '';
    return date.toLocaleDateString();
};

const Helper = {
    capitalizeString,
    formatNumber,
    formatDate,
};

export default Helper;
