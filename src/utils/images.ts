export const AssetImages = {
  LAYER: require("../assets/images/layer.png"),
  LOADING: require("../assets/images/loading.gif"),
  CAT: require("../assets/images/cat.jpg"),
  AVATAR: require("../assets/images/avatar.png"),
};
