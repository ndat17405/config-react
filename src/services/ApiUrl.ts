// Define your API base URL
const API_BASE_URL = "http://localhost:8080/api/v1";

export const ApiUrl = {
  // ========================== Sprint 1 ========================== //
  SIGN_IN: `${API_BASE_URL}/auth/sign-in`,
  SIGN_UP: `${API_BASE_URL}/auth/sign-up`,
  SIGN_OUT: `${API_BASE_URL}/auth/sign-out`,

};
