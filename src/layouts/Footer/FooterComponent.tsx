import React from "react";
import "./FooterComponent.scss";

function FooterComponent() {
  return (
    <div className="container-footer">
      <div className="information-footer">
        <span>
          Mappe Technology Joint Stock Company, a member of Mappe Group
        </span>
        <br />
        <br />
        Business registration number: 54564322435 - Date of issue: May 13, 2014,
        amended for the 100th time, June 8, 2023. <br />
        <br />
        Cơ quan cấp: Sở kế hoạch và đầu tư TPHCM. <br />
        <br />
        Địa chỉ: Tầng 4 toà nhà Beta trường đại học FPT, số 600 đường Nguyễn Văn
        Cừ, quận Ninh Kiều, thành phố Cần Thơ, Việt Nam. <br />
        <br />
        Email: mappe.help@gmail.com
      </div>
      <div className="contact-footer">
        <span>Please leave your email, if you want us to contact you</span>
        <br />
        <br />
        <input type="text" placeholder="Your email" />
        <button>Send</button>
      </div>
    </div>
  );
}

export default FooterComponent;
