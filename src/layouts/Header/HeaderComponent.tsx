import React, { useEffect } from "react";
import "./HeaderComponent.scss";
import WidgetsIcon from "@mui/icons-material/Widgets";
import LocalMallIcon from "@mui/icons-material/LocalMall";
import SearchIcon from "@mui/icons-material/Search";
import { useSelector } from "react-redux";
import { Avatar, IconButton, Menu, MenuItem } from "@mui/material";
import { AssetImages } from "../../utils/images";
import Text from "../../components/Text/Text";

function HeaderComponent() {
  const account = useSelector((state: any) => state.auth.account);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => { }, [account]);

  return (
    <div className="header">
      <div className="col">
        <h1>Mappe</h1>
      </div>
      <div className="col menu">
        <WidgetsIcon />
      </div>
      <div className="col-row">
        <input type="text" placeholder="Search in Mappe" />
        <div className="btn-search">
          <SearchIcon />
        </div>
      </div>
      <div className="col cart">
        <LocalMallIcon />
      </div>
      <div className="col avatar">
        <div>
          <IconButton
            size="medium"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleMenu}
            color="inherit"
          >
            <Avatar
              alt="avatar"
              src={AssetImages.AVATAR}
              sx={{ width: 32, height: 32 }}
            />
            <Text
              title={account?.name}
              property={"profile-name"}
              onPress={handleClose}
            />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem >Profile</MenuItem>
            <MenuItem >Sign Out</MenuItem>
          </Menu>
        </div>
        {/* <div className="btn-sign-out" onClick={signOut}>
          Sign Out
        </div> */}
      </div>
    </div>
  );
}

export default HeaderComponent;
