import React from "react";
import { AssetImages } from "../../utils/images";
import "./Loading.Style.scss";

const LoadingComponent: React.FC<{}> = () => {
  return (
    <div className="loading">
      <img src={AssetImages.LOADING} alt="loading" />
    </div>
  );
};

export default LoadingComponent;
