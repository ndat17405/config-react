import { HomeActions } from '../../redux/actions';

export interface SaveListProductAction {
  type: HomeActions.SAVE_LIST_PRODUCT;
  payload: any;
}

export type HomeAction = SaveListProductAction;

export const saveListProduct = (listProduct: any): SaveListProductAction => {
  return {
    type: HomeActions.SAVE_LIST_PRODUCT,
    payload: listProduct
  };
};
