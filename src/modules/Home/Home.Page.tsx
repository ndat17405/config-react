import "./Home.Style.scss";
import BoxContainer from "../../components/Box/Box.Container";
import HeaderComponent from "../../layouts/Header/HeaderComponent";
import FooterComponent from "../../layouts/Footer/FooterComponent";

const HomePage: React.FC<{}> = () => {

  return (
    <BoxContainer property="all">
      {/* header */}
      <HeaderComponent />

      {/* Footer */}
      <FooterComponent />
    </BoxContainer>
  );
};

export default HomePage;
