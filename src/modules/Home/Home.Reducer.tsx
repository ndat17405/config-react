import { HomeActions } from "../../redux/actions";

export interface HomeState {
  listProduct: any | [];
}

const initialState = {
  listProduct: [],
};

function homeReducer(state = initialState, action: any) {
  switch (action.type) {
    case HomeActions.SAVE_LIST_PRODUCT:
      return {
        ...state,
        listProduct: action.payload,
      };
    default:
      return state;
  }
}

export default homeReducer;
