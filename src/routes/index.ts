export const routes = {
  home: {
    root: "/",
  },
  auth: {
    root: "/auth",
    signIn: "/auth/sign-in",
    signUp: "/auth/sign-up",
    forgot: "/auth/forgot",
  },
};
