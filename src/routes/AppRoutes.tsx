import React from "react";
import { Route, Routes } from "react-router-dom";
import { routes } from ".";
import HomeContainer from "../modules/Home/Home.Container";

function AppRoutes() {
  return (
    <Routes>
      <Route path={routes.home.root} element={<HomeContainer />} />
    </Routes>
  );
}

export default AppRoutes;
